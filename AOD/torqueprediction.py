import matplotlib.pyplot as plt
from AOD.Material import *
from AOD.Model import *
from AOD.Bot import *
import warnings


def main():
    warnings.filterwarnings('ignore')
    model = Model()
    model.world.Layers['Soil'] = Mud()
    model.world.Layers['Fluid'].v = np.array([0., -1., 0.]) * ureg['m/s']
    model.bot.v = np.arange(0., 1., step=0.1) * ureg['m/s']
    # model.bot.set_buoyancy(volume=0.5 * ureg['m**3'], layers=model.world.Layers)
    results = model.solve_sinkdepth()
    print('P allow: ' + str(results[4].to('kPa')))
    print('sink depth: ' + str(results[3].to('mm')))
    model.print_sinkdepth(results)
    torque_req = model.solve_torque(results)
    print(torque_req[0] / 2)
    model.print_torque(torque_req, model.bot.v, 'Bot speed [m/s]')



if __name__ == '__main__':
    main()
