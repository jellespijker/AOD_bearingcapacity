from pint import UnitRegistry, set_application_registry
from math import cos, sin

ureg = UnitRegistry(autoconvert_offset_to_baseunit=True)  # Allows for unit save calculations
Q_ = ureg.Quantity  # Allows for custom quantities to be registered
g = 9.80665 * ureg['m/s**2']  # Gravitational constant


class Force(object):
    _force = 0. * ureg['N']
    _angle = 0. * ureg['rad']

    def __add__(self, other):
        return self.force + other.force

    @property
    def force(self):
        return self._force

    @force.setter
    def force(self, value):
        self._force = value.to('N')

    @property
    def angle(self):
        return self._angle

    @angle.setter
    def angle(self, value):
        self._angle = value.to('rad')

    @property
    def x(self):
        return self.force * cos(self.angle)

    @property
    def y(self):
        return self.force * sin(self.angle)
